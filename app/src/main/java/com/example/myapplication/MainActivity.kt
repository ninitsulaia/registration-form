package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordAgain: EditText
    private lateinit var buttonRegistration: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPasswordAgain = findViewById(R.id.editTextPasswordAgain)
        buttonRegistration = findViewById(R.id.buttonRegistration)

        buttonRegistration.setOnClickListener {
            val mail = editTextEmail.text.toString().trim()
            val password = editTextPassword.text.toString().trim()
            val passwordagain = editTextPasswordAgain.text.toString().trim()

            if (mail.isEmpty() ){
                editTextEmail.error = "შეიყვანეთ მეილი"
                return@setOnClickListener
            }else if (!(mail.contains("@"))){
                editTextEmail.error = "შეიყვანეთ სწორი მეილი"
                return@setOnClickListener
            }else if (password.isEmpty()){
                editTextPassword.error = "შეიყვანეთ პაროლი"
                return@setOnClickListener
            }else if (!(password.matches(".*[!@#$%^&*()].*".toRegex())) ||
                !(password.matches(".*[1234567890].*".toRegex())) ||
                !(password.matches(".*[QWERTYUIOPASDFGHJKLZXCVBNM].*".toRegex()))) {
                editTextPassword.error = "შეიყვანეთ სათანადო პარპოლი"
                return@setOnClickListener
            }else if (password.length <= 9){
                editTextPassword.error = "არასათანადო სიმბოლოების რაოდენობა"
                return@setOnClickListener
            }else if (passwordagain != password){
                editTextPasswordAgain.error = "შეყვანილი პაროლი არ ემთხვევა"
                return@setOnClickListener
            }else
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(mail, password)
                    .addOnCompleteListener{ task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "წარმატებით დარეგისტრირდი", Toast.LENGTH_SHORT).show()
                        }
                        else
                            Toast.makeText(this, "ვერ დარეგისტრირდი", Toast.LENGTH_SHORT).show()
                    }

        }
    }

}
